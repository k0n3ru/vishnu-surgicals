import React from 'react'

import Layout from '../components/layout'
import SEO from '../components/seo'
import VSLink from '../components/vslink'
import Footer from '../components/footer'
import HomePageItems from '../components/home-page-item'
import SiteData from '../components/site-data-query'

import { Icon } from 'semantic-ui-react'

const NotFoundPage = () => {
  const mainPanels = [
    {
      id: 'products',
      title: 'Products',
      href: '/products',
      description: 'See all available products',
    },
    {
      id: 'brands',
      title: 'Brands',
      href: '/brands',
      description: 'See all available brands',
    },
  ]

  const { siteMetadata } = SiteData()

  return (
    <Layout>
      <SEO title="404: Not found" />
      <p>Page you are looking for is not found. Sorry about that</p>
      <p>
        Contact us at{' '}
        <VSLink href={'tel:' + siteMetadata.phone} size="large">
          <Icon name="phone square" size="large" /> {siteMetadata.phone}
        </VSLink>{' '}
        or{' '}
        <VSLink href={'mailto:' + siteMetadata.email} size="large">
          <Icon name="mail" size="large" /> {siteMetadata.email}
        </VSLink>
      </p>
      <HomePageItems data={mainPanels} />
      <Footer />
    </Layout>
  )
}

export default NotFoundPage
