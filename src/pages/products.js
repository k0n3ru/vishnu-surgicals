import React from 'react'
import Layout from '../components/layout'
import ProductsQuery from '../components/products-query'
import { ProductsList } from '../components/products-list'

const ProductPage = () => {
  const data = ProductsQuery()
  return (
    <Layout>
      <ProductsList data={data} />
    </Layout>
  )
}

export default ProductPage
