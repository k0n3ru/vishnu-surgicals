import React from 'react'
import Layout from '../components/layout'
import SEO from '../components/seo'
import Footer from '../components/footer'
import { BrandShowcase } from '../components/brand-showcase'
import { ProductTypeShowcase } from '../components/categories-showcase'
import PopularProducts from '../components/popular-products'
import Banner from '../components/homepage-banner'
import { Grid } from 'semantic-ui-react'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Grid centered>
        <Grid.Row>
          <Grid.Column>
            <Banner />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <PopularProducts />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <ProductTypeShowcase />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <BrandShowcase />
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <Footer />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Layout>
  )
}

export default IndexPage
