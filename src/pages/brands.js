import React from 'react'
import Layout from '../components/layout'
import Brands from '../components/brands'

const BrandsPage = () => {
  return (
    <Layout>
      <Brands />
    </Layout>
  )
}

export default BrandsPage
