import React, { Component } from 'react'
import 'semantic-ui-css/semantic.min.css'
import Link from './vslink'
import { Menu, Container, Dropdown, Accordion } from 'semantic-ui-react'
import ProductTypesQuery from './product-types-query'
import BrandsQuery from './brands-query'
import SiteData from './site-data-query'

//import PropTypes from 'prop-types'
//TODO: Set active status on menu items.
//TODO: Add PropTypes to VSMenu
//TODO: Pluralize product ttype links

class MobileMenu extends Component {
  render() {
    const panels = [
      {
        key: 'categories',
        title: 'Categories',
        content: { content: <ProductTypesMenuItems /> },
      },
      {
        key: 'brands',
        title: 'Brands',
        content: { content: <BrandsMenuItems /> },
      },
    ]

    return (
      <Container>
        <Menu.Item as={Link} to="/" header>
          Home
        </Menu.Item>
        <Accordion defaultActiveIndex={0} panels={panels} />
      </Container>
    )
  }
}

class VSMenu extends Component {
  render() {
    const { mobile, data } = this.props

    if (mobile) {
      return <MobileMenu data={data} />
    }

    return <DesktopMenu data={data} />
  }
}

const ProductTypesMenuItems = () => {
  const productTypes = ProductTypesQuery()
  return productTypes.map(pt => (
    <Menu.Item as={Link} key={Math.random()} to={pt.href}>
      {pt.title}
    </Menu.Item>
  ))
}

const BrandsMenuItems = () => {
  const brands = BrandsQuery()
  return brands.map(brand => (
    <Menu.Item as={Link} key={Math.random()} to={brand.href}>
      {brand.title}
    </Menu.Item>
  ))
}

const DesktopMenu = () => {
  const { siteMetadata } = SiteData()
  return (
    <Menu
      pointing
      secondary
      fluid
      widths={5}
      style={{ backgroundColor: 'white' }}
    >
      <Dropdown compact text="Products" pointing className="link item">
        <Dropdown.Menu>
          <Dropdown.Header>Categories</Dropdown.Header>
          <ProductTypesMenuItems />
        </Dropdown.Menu>
      </Dropdown>
      <Dropdown text="Brands" pointing className="link item">
        <Dropdown.Menu>
          <BrandsMenuItems />
        </Dropdown.Menu>
      </Dropdown>
      <Menu.Item as={Link} to="/" header>
        {siteMetadata.title}
      </Menu.Item>
      <Menu.Item>Offers</Menu.Item>
      <Menu.Item>Contact Us</Menu.Item>
    </Menu>
  )
}

const Header = VSMenu

export default Header
