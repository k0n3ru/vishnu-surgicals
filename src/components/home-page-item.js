import React from 'react'
import { Showcase } from './home-page-showcase'

const HomePageItems = ({ data }) => {
  const showData = {
    title: '',
    items: data,
  }

  return <Showcase data={showData} />
}

export default HomePageItems
