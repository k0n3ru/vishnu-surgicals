import { useStaticQuery, graphql } from 'gatsby'

const SiteData = () => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            phone
            email
            city
          }
        }
      }
    `
  )
  return site
}

export default SiteData
