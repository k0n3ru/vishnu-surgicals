import React, { Component } from 'react'

export default class Dimmer extends Component {
  state = {}

  active = () => this.setState({ opacity: 0.75 })
  notActive = () => this.setState({ opacity: 1 })

  render() {
    const { opacity } = this.state
    const { children } = this.props
    return (
      <div
        style={{ opacity: opacity }}
        onMouseEnter={this.active}
        onMouseLeave={this.notActive}
        onTouchStart={this.active}
        onTouchEnd={this.notActive}
      >
        {children}
      </div>
    )
  }
}
