import { useStaticQuery, graphql } from 'gatsby'
import { getDir, getImagesForFilter } from './utils'

const ProductsQuery = () => {
  const { allMarkdownRemark, allFile } = useStaticQuery(
    graphql`
      query {
        allMarkdownRemark {
          edges {
            node {
              id
              frontmatter {
                title
                brand
                model
                type
                colors
                sizes
              }
              excerpt
              fileAbsolutePath
            }
          }
        }
        allFile(filter: { extension: { regex: "/(png|jpeg|jpg)$/" } }) {
          edges {
            node {
              id
              absolutePath
              base
              relativeDirectory
              relativePath
              childImageSharp {
                fluid(maxHeight: 150) {
                  ...GatsbyImageSharpFluid
                  presentationWidth
                }
                fixed(width: 200) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    `
  )
  return allMarkdownRemark.edges.map(e => {
    return {
      ...e.node.frontmatter,
      id: e.node.id,
      excerpt: e.node.excerpt,
      href: getDir(e.node.fileAbsolutePath),
      description: e.node.frontmatter.model + ' ' + e.node.frontmatter.type,
      images: getImagesForFilter(e.node.fileAbsolutePath, allFile),
    }
  })
}

export default ProductsQuery
