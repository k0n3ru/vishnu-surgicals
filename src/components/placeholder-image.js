import { useStaticQuery, graphql } from 'gatsby'

const PlaceholderImageFluid = () => {
  const { allFile } = useStaticQuery(
    graphql`
      query {
        allFile(filter: { relativePath: { eq: "placeholder.png" } }) {
          nodes {
            childImageSharp {
              fluid(maxWidth: 400) {
                ...GatsbyImageSharpFluid
                presentationWidth
              }
            }
          }
        }
      }
    `
  )
  //<Img fluid={allFile.edges[0].node.childImageSharp.fluid} />
  return allFile.nodes[0].childImageSharp.fluid
}

export default PlaceholderImageFluid
