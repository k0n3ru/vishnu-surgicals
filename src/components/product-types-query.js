import { useStaticQuery, graphql } from 'gatsby'
import { typeUrlForType } from './utils'

const ProductTypesQuery = () => {
  const { allMarkdownRemark } = useStaticQuery(
    graphql`
      query {
        allMarkdownRemark {
          types: distinct(field: frontmatter___type)
        }
      }
    `
  )
  return allMarkdownRemark.types.map(e => {
    return {
      title: e + 's',
      href: typeUrlForType(e),
      description: 'Figure out a way to describe product types',
    }
  })
}

export default ProductTypesQuery
