import React from 'react'
import VSLink from './vslink'
import { Grid } from 'semantic-ui-react'
import ImageOrCard from './image-or-card'
import { Header } from 'semantic-ui-react'

export const Showcase = ({ data }) => (
  <Grid centered>
    <Grid.Row columns={1}>
      <Grid.Column>
        <Header basic="true">{data.title}</Header>
      </Grid.Column>
    </Grid.Row>
    <Grid.Row stretched>
      {data.items.map(item => (
        <VSLink key={item.href} to={item.href}>
          <ImageOrCard data={item}></ImageOrCard>
        </VSLink>
      ))}
    </Grid.Row>
  </Grid>
)
