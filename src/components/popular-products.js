import React from 'react'
import ProductsQuery from '../components/products-query'
import { Showcase } from './home-page-showcase'

const PopularProducts = () => {
  const items = ProductsQuery().slice(0, 8)
  const data = {
    title: 'Popular Products',
    items: items,
  }
  return <Showcase data={data} />
}

export default PopularProducts
