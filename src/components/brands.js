import React from 'react'
import HomePageItems from './home-page-item'
import BrandsQuery from './brands-query'

const Brands = props => {
  const data = BrandsQuery()
  return <HomePageItems {...props} data={data} />
}

export default Brands
