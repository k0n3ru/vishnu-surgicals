import React, { Component } from 'react'
import './slider.css'

class Slider extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      lastIndex: 0,
      transition: false,
    }
  }

  static defaultProps = {
    loop: false,
    selected: 0,
    showArrows: true,
    showNav: true,
  }

  componentDidMount() {
    this.interval = setInterval(() => this.goToNextSlide(), 3000)
  }
  componentWillUnmount() {
    clearInterval(this.interval)
  }

  componentWillMount = () => {
    const { selected, loopFrequency } = this.props

    this.setState({
      index: selected,
      lastIndex: selected,
      loopFrequency: loopFrequency,
    })
  }

  componentWillReceiveProps = nextProps => {
    const { selected } = this.props

    if (selected !== nextProps.selected) {
      this.goToSlide(nextProps.selected)
    }
  }

  goToNextSlide = () => {
    this.goToSlide(this.state.index + 1, null)
  }

  goToSlide = (index, event) => {
    const { children, loop } = this.props

    if (event) {
      event.preventDefault()
      event.stopPropagation()
    }

    if (index < 0) {
      index = loop ? children.length - 1 : 0
    } else if (index >= children.length) {
      index = loop ? 0 : children.length - 1
    }

    this.setState({
      index: index,
      lastIndex: index,
      transition: true,
    })
  }

  renderNav = () => {
    const { children } = this.props
    const { lastIndex } = this.state

    const nav = children.map((slide, i) => {
      const buttonClasses =
        i === lastIndex
          ? 'Slider-navButton Slider-navButton--active'
          : 'Slider-navButton'
      return (
        <button
          className={buttonClasses}
          key={i}
          onClick={event => this.goToSlide(i, event)}
        />
      )
    })

    return <div className="Slider-nav">{nav}</div>
  }

  renderArrows = () => {
    const { children, loop, showNav } = this.props
    const { lastIndex } = this.state
    const arrowsClasses = showNav
      ? 'Slider-arrows'
      : 'Slider-arrows Slider-arrows--noNav'

    return (
      <div className={arrowsClasses}>
        {loop || lastIndex > 0 ? (
          <button
            className="Slider-arrow Slider-arrow--left"
            onClick={event => this.goToSlide(lastIndex - 1, event)}
          />
        ) : null}
        {loop || lastIndex < children.length - 1 ? (
          <button
            className="Slider-arrow Slider-arrow--right"
            onClick={event => this.goToSlide(lastIndex + 1, event)}
          />
        ) : null}
      </div>
    )
  }

  render() {
    const { children, showArrows, showNav } = this.props

    const { index, transition } = this.state

    const slidesStyles = {
      width: `${100 * children.length}%`,
      transform: `translateX(${-1 * index * (100 / children.length)}%)`,
    }
    const slidesClasses = transition
      ? 'Slider-slides Slider-slides--transition'
      : 'Slider-slides'

    return (
      <div className="Slider" ref="slider">
        {showArrows ? this.renderArrows() : null}
        {showNav ? this.renderNav() : null}

        <div className="Slider-inner">
          <div className={slidesClasses} style={slidesStyles}>
            {children}
          </div>
        </div>
      </div>
    )
  }
}

export default Slider
