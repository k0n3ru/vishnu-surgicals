import React from 'react'
import PropTypes from 'prop-types'

import DesktopContainer from './desktop-container'
import MobileContainer from './mobile-container'
import SiteData from './site-data-query'
import { getWidth } from './utils'
import NoSSR from 'react-no-ssr'

const ResponsiveContainer = ({ children, getWidth, siteMetadata }) => (
  <NoSSR>
    <DesktopContainer getWidth={getWidth}>{children}</DesktopContainer>
    <MobileContainer getWidth={getWidth} siteMetadata={siteMetadata}>
      {children}
    </MobileContainer>
  </NoSSR>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}

const Layout = ({ children }) => {
  const { siteMetadata } = SiteData()
  return (
    <div
      style={{
        margin: `10px auto`,
        maxWidth: 960,
        padding: `0px 1.0875rem 1.45rem`,
        paddingTop: 0,
      }}
    >
      <ResponsiveContainer getWidth={getWidth()} siteMetadata={siteMetadata}>
        {children}
      </ResponsiveContainer>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
