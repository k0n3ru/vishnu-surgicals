import { useStaticQuery, graphql } from 'gatsby'
import { brandUrlForBrand } from './utils'

const AllImages = () => {
  const { allFile } = useStaticQuery(
    graphql`
      query {
        allFile(filter: { extension: { regex: "/(png|jpeg|jpg)$/" } }) {
          edges {
            node {
              id
              absolutePath
              base
              relativeDirectory
              relativePath
              childImageSharp {
                fluid(maxWidth: 300) {
                  ...GatsbyImageSharpFluid
                }
                fixed(width: 200) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    `
  )
  //<Img fluid={allFile.edges[0].node.childImageSharp.fluid} />
  return allFile
}

export default AllImages
