import React from 'react'
import { Showcase } from './home-page-showcase'
import BrandsQuery from './brands-query'

export const BrandShowcase = ({ props, data }) => {
  const allBrands = BrandsQuery()
  const brands = {
    title: 'Top Brands',
    items: allBrands,
  }
  return <Showcase data={brands} />
}
