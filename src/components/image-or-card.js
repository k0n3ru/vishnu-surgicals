import React from 'react'
import { Grid, Card, Header } from 'semantic-ui-react'
import Dimmer from './dimmer'

const ImageOrCard = ({ data }) => {
  const square = { width: 200, height: 200, margin: '10px', padding: '10px' }
  return (
    <Dimmer>
      <Grid.Column style={square}>
        {data.images ? (
          <div>
            <div>{data.images[0]}</div>
            <Header size="tiny" sub>
              {data.title}
            </Header>
          </div>
        ) : (
          <Card color="grey" header={data.title} centered />
        )}
      </Grid.Column>
    </Dimmer>
  )
}

export default ImageOrCard
