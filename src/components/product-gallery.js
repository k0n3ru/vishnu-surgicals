import React, { Component } from 'react'
import Img from 'gatsby-image'
import { Grid } from 'semantic-ui-react'
import PlaceholderImageFluid from './placeholder-image'
import NonStretchedFluidImage from './non-stretched-fluid-image'

class Gallery extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeIndex: 0,
    }
  }

  handleClick = index => {
    this.setState({
      activeIndex: index,
    })
  }

  componentWillMount = () => {
    const { activeIndex } = this.state
    const { nodes } = this.props
    if (activeIndex === 0) this.setState({ activeIndex: nodes[0].id })
  }

  render() {
    const { nodes } = this.props
    const { activeIndex } = this.state
    return (
      <Grid divided="vertically">
        <Grid.Row columns={1}>
          <Grid.Column>
            {nodes
              .filter(n => n.id === activeIndex)
              .map(n => (
                <NonStretchedFluidImage key={n.id} fluid={n.src} />
              ))}
          </Grid.Column>
        </Grid.Row>
        <Grid.Row columns={nodes.length}>
          {nodes.map(node => {
            return (
              <Grid.Column
                key={node.id}
                onClick={event => this.handleClick(node.id)}
              >
                <Img fluid={node.src} style={{ maxWidth: 60 }} />
              </Grid.Column>
            )
          })}
        </Grid.Row>
      </Grid>
    )
  }
}

const ProductGallery = ({ allFile }) => {
  const placeholder = PlaceholderImageFluid()

  const nodes =
    allFile.edges.length === 0
      ? [{ id: Math.random(), src: placeholder }]
      : allFile.edges.map(({ node }) => {
          return {
            id: node.id,
            src: node.childImageSharp.fluid,
          }
        })
  return <Gallery nodes={nodes} />
}

export default ProductGallery
