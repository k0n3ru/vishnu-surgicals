import React from 'react'
import PlaceholderImageFluid from './placeholder-image'
import path from 'path'
import NonStretchedFluidImage from './non-stretched-fluid-image'
import { Responsive } from 'semantic-ui-react'

export const getDir = filePath => {
  return path
    .dirname(filePath)
    .split('/')
    .pop()
}

export const capitalize = string => {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export const urlify = string => {
  return string
    .replace(/[^a-z0-9_]+/gi, '-')
    .replace(/^-|-$/g, '')
    .toLowerCase()
}

export const brandUrlForBrand = brandname => {
  return '/brands/' + urlify(brandname)
}

export const typeUrlForType = type => {
  return '/category/' + urlify(type)
}

export const DummyImage = () => {
  return <NonStretchedFluidImage fluid={PlaceholderImageFluid()} />
}

export const getImagesForFilter = (path, allFile) => {
  const relativePath = getDir(path)
  const pathFilter = ({ node }) => node.relativeDirectory === relativePath
  const nodes = allFile.edges.filter(pathFilter).map(e => e.node)
  if (nodes.length === 0) return Array.of(<DummyImage />)
  return nodes.map(node => (
    <NonStretchedFluidImage key={node.id} fluid={node.childImageSharp.fluid} />
  ))
}

const getWidthFactory = isMobileFromSSR => () => {
  const isSSR = typeof window === 'undefined'
  const ssrValue = isMobileFromSSR
    ? Responsive.onlyMobile.maxWidth
    : Responsive.onlyTablet.minWidth

  return isSSR ? ssrValue : window.innerWidth
}

const isMobileFromSSR = () => {
  // const md = new MobileDetect(req.headers["user-agent"]);
  // const isMobileFromSSR = !!md.mobile();
  //https://github.com/Semantic-Org/Semantic-UI-React/issues/3361
  return true
}

export const getWidth = () => {
  return getWidthFactory(isMobileFromSSR())
}
