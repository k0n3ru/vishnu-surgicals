import React from 'react'
import { Responsive, Grid } from 'semantic-ui-react'
import Header from './header'
import SiteData from './site-data-query'

const DesktopContainer = ({ children, getWidth }) => (
  <Responsive
    fireOnMount
    getWidth={getWidth}
    minWidth={Responsive.onlyTablet.minWidth}
  >
    <Grid centered columns={1}>
      <Grid.Row>
        <Grid.Column>
          <Header siteMetadata={SiteData()} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row></Grid.Row>
      <Grid.Row>
        <Grid.Column>{children}</Grid.Column>
      </Grid.Row>
    </Grid>
  </Responsive>
)

export default DesktopContainer
