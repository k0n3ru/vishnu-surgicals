import React from 'react'
import SiteData from './site-data-query'
import Img from 'gatsby-image'
import { useStaticQuery, graphql } from 'gatsby'

import { Header } from 'semantic-ui-react'

const LogoQuery = () => {
  const data = useStaticQuery(
    graphql`
      query {
        file(relativePath: { eq: "icon.png" }) {
          childImageSharp {
            fixed(height: 30) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    `
  )
  return data.file.childImageSharp.fixed
}

export const LogoTitle = ({ props }) => {
  const { siteMetadata } = SiteData()
  return (
    <Header>
      <Img fixed={LogoQuery()} style={{ marginRight: '5px' }}></Img>
      <Header.Content>{siteMetadata.title}</Header.Content>
    </Header>
  )
}
