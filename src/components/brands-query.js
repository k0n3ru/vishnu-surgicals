import { useStaticQuery, graphql } from 'gatsby'
import { brandUrlForBrand } from './utils'

const BrandsQuery = () => {
  const { allMarkdownRemark } = useStaticQuery(
    graphql`
      query {
        allMarkdownRemark {
          brands: distinct(field: frontmatter___brand)
        }
      }
    `
  )
  return allMarkdownRemark.brands.map(e => {
    return {
      id: e,
      title: e,
      href: brandUrlForBrand(e),
      description: 'Figure out a way to describe brands',
    }
  })
}

export default BrandsQuery
