import React from 'react'
import { Showcase } from './home-page-showcase'
import ProductTypesQuery from './product-types-query'

export const ProductTypeShowcase = ({ props, data }) => {
  const allCategories = ProductTypesQuery()
  const topCategories = {
    title: 'Top Categories',
    items: allCategories,
  }
  return <Showcase data={topCategories} />
}
