import React from 'react'
import HomePageItems from './home-page-item'

export const ProductsList = ({ props, data }) => {
  return <HomePageItems {...props} data={data} />
}
