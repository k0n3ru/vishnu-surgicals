import PropTypes from 'prop-types'
import React, { Component } from 'react'
import 'semantic-ui-css/semantic.min.css'
import { Icon, Menu, Responsive, Sidebar, Grid } from 'semantic-ui-react'
import Header from './header'
import Link from './vslink'

class MobileContainer extends Component {
  state = {}

  handleSidebarHide = () => this.setState({ sidebarOpened: false })

  handleToggle = () => this.setState({ sidebarOpened: true })

  render() {
    const { children, getWidth, siteMetadata } = this.props
    const { sidebarOpened } = this.state

    return (
      <Responsive
        as={Sidebar.Pushable}
        fireOnMount
        getWidth={getWidth}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <Sidebar
          as={Menu}
          animation="push"
          onHide={this.handleSidebarHide}
          vertical
          visible={sidebarOpened}
        >
          <Header mobile />
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened}>
          <Grid centered columns={1}>
            <Grid.Row>
              <Grid.Column fixed="top">
                <Menu
                  pointing
                  secondary
                  fluid
                  fixed="top"
                  style={{ backgroundColor: 'white', fixed: 'top' }}
                >
                  <Menu.Item onClick={this.handleToggle}>
                    <Icon name="sidebar" size="large" />
                  </Menu.Item>
                  <Menu.Item as={Link} to="/">
                    <h4>{siteMetadata.title}</h4>
                  </Menu.Item>
                  <Menu.Item position="right">
                    <Link href={'tel:' + siteMetadata.phone}>
                      <Icon name="phone square" size="large" color="black" />
                    </Link>
                  </Menu.Item>
                  <Menu.Item>
                    <Link href={'mailto:' + siteMetadata.email}>
                      <Icon name="mail" size="large" color="black" />
                    </Link>
                  </Menu.Item>
                </Menu>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row></Grid.Row>
            <Grid.Row>
              <Grid.Column>{children}</Grid.Column>
            </Grid.Row>
          </Grid>
        </Sidebar.Pusher>
      </Responsive>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

export default MobileContainer
