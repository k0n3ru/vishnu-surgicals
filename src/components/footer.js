import React from 'react'
import {
  Grid,
  Container,
  Divider,
  Header,
  List,
  Segment,
} from 'semantic-ui-react'
import SiteData from './site-data-query'

// const PlaceholderExampleParagraph = () => (
//   <Placeholder fluid>
//     <Placeholder.Paragraph>
//       <Placeholder.Line />
//       <Placeholder.Line />
//       <Placeholder.Line />
//       <Placeholder.Line />
//     </Placeholder.Paragraph>
//   </Placeholder>
// )

const Footer = () => {
  const { siteMetadata } = SiteData()
  return (
    <Segment
      inverted
      vertical
      style={{ margin: '5em 0em 0em', padding: '5em 0em' }}
    >
      <Container textAlign="center">
        <Grid divided inverted stackable>
          <Grid.Column width={3}>
            <Header inverted as="h4" content="Partners" />
            <List link inverted>
              <List.Item as="a">Karma</List.Item>
              <List.Item as="a">Tynor</List.Item>
              <List.Item as="a">Omron</List.Item>
              <List.Item as="a">Jhonson & Jhonson</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={3}>
            <Header inverted as="h4" content="Custom Products" />
            <List link inverted>
              <List.Item as="a">Bed Frames</List.Item>
              <List.Item as="a">Footwear</List.Item>
              <List.Item as="a">Wlakers</List.Item>
              <List.Item as="a">Aprons</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={3}>
            <Header inverted as="h4" content="Services" />
            <List link inverted>
              <List.Item as="a">Warranty</List.Item>
              <List.Item as="a">Repair</List.Item>
              <List.Item as="a">Spares</List.Item>
              <List.Item as="a">Wholesale</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={7}>
            <Header inverted as="h4" content="" />
            <p>
              We also offer shipping locally and to towns and villages around
              Vijayawada.
            </p>
          </Grid.Column>
        </Grid>

        <Divider inverted section />
        <List horizontal inverted divided link size="small">
          <List.Item as="a" href="/sitemap.xml">
            Site Map
          </List.Item>
          <List.Item as="a" href="#">
            Contact Us
          </List.Item>
          <List.Item as="a" href="#">
            Terms and Conditions
          </List.Item>
          <List.Item as="a" href="#">
            {siteMetadata.title}
          </List.Item>
        </List>
      </Container>
    </Segment>
  )
}

export default Footer
