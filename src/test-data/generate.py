import os
import random
from faker import Faker
from faker.providers import lorem,internet,company,address,automotive

fake = Faker()
fake.add_provider(internet)
fake.add_provider(company)
fake.add_provider(address)
fake.add_provider(lorem)
fake.add_provider(automotive)


companies = [fake.company() for x in range(10)]
types = [fake.word() for x in range(10)]


def createFolder(directory):

    company = random.choice(companies)
    model = fake.license_plate()
    typ = random.choice(types)

    title = company + ' ' + model + ' ' + typ

    bs = title.replace(" ", "-")
    direc = directory + "dummy-" + bs

    if not os.path.exists(direc):
        os.makedirs(direc)
    filename = direc + "/" + bs + ".md"
    with open(filename, 'w') as f: 
        f.write('---')
        f.write('\n')
        f.write('title: "' + title + '"')
        f.write('\n')
        f.write('brand: "' + company + '"')
        f.write('\n')
        f.write('type: "' + typ + '"')
        f.write('\n')
        f.write('model: "' + model + '"')
        f.write('\n')
        f.write('---')
        f.write('\n')
        f.write('\n')
        f.write( '"' + fake.text() + '"')
        f.write('\n')

for i in range(300):
    createFolder('../content/products/')

