---
brand: "Omron"
type: "Nebuliser"
model: "C803"
title: "Omron c803 Nebuliser"

---

OMRON CompAIR Basic C803 is the lightest and quietest OMRON compressor nebulizer for the treatment of lower airways respiratory diseases like light asthma.
This nebulizer provides an excellent value for money for consumers who look for a basic compressor nebulizer.