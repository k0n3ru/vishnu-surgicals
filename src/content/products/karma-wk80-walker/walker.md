---
brand: "Karma"
type: "Walker"
model: "WK-80"
title: "Karma WK-80 Walker"
sizes: 73-90cm(M), 78-95cm(L)
---

Karma lightweight aluminum folding walker features the eye-catching appearance and two-in-one fixed and reciprocal functions. Our walker series have a wide variety of choices for every individual’s need.