---
brand: "Omron"
type: "Activity Monitor"
model: "Walking style one 2.0"
title: "Omron walking style one 2.0 Activity Monitor"

---

Tracking your steps just became easier. The Walking style One 2.0 is just 3,8 mm thin and is made to be worn on the hip, carried in your pocket or even in a bag to count your walking activity. The 3-dimensional electronic sensor technology guarantees you OMRON's most accurate way of measurement. Reach for a goal of 10.000 steps a day and monitor your progress!