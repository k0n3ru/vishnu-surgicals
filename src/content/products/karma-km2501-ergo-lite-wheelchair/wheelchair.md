---
brand: "Karma"
type: "Wheelchair"
model: "km-2501 Ergo lite"
title: "Karma km-2501 Ergo lite Wheelchair"
colors: "Silver Pearl"
---

Weighing only 8.5kg, the Ergo Lite (KM-2501) is Karma’s lightest attendant-propelled wheelchair. The compact design is ideal for wheelchair users who need a simple, lightweight wheelchair for short trips. It is equipped with Karma’s unique S-Ergo seating system.

