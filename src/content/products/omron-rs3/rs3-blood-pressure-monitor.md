---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Rs3"
title: "Omron rs3 BP Monitor"

---

Now you can be trebly sure that you have taken an accurate reading with RS3’s advanced analytical technology. It automatically displays the average of up to the last 3 readings taken over a 10 minute period. Another sensor also highlights any movement (which can invalidate readings) to ensure the accuracy of every individual result. All of which gives you a formidable level of accuracy for a wrist monitor.