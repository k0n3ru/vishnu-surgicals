---
brand: "Omron"
type: "Activity Monitor"
model: "Walking style iv white"
title: "Omron walking style iv white Activity Monitor"

---

The Walking style IV uses a highly accurate 3D sensor to help you track your steps and how many calories you burn. It automatically detects your active, brisk steps from your normal walking routine. Next to that it comes with an 'action' mode that allows you to track a particular activity or time separately from your normal daily routine.
HJ-325-EB (blue)
HJ-325-EBK (black)
HJ-325-EW (white)