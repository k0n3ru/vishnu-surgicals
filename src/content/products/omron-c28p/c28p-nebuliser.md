---
brand: "Omron"
type: "Nebuliser"
model: "C28p"
title: "Omron c28p Nebuliser"

---

OMRON CompAIR C28P has a robust design for intensive use, suitable for the treatment of lower airways respiratory diseases like asthma, COPD, cystic  fybrosis or emphysema. 
It is a highly efficient nebulizer that combines short inhalation time with efficient medication delivery, thanks to its powerful compressor and Virtual Valve Technology (V.V.T.).
The high efficiency from V.V.T. adapts to your breathing, thus matching your breathing pattern. This technology reduces drug wastage while breathing out and maximizes drug availability when breathing in.  Also, this nebulizer kit is made of easy to clean polypropylene without the use of silicon valves which are commonly used in conventional nebulizers.

High efficiency with the Virtual Valve Technology
Powerful compressor for high efficacy
Shorter inhalation time
Robust for frequent use
Compliant with a wide range of medication
Suitable for use with children with simple one-button operation
