---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Rs7 intelli it"
title: "Omron rs7 intelli it BP Monitor"

---

OMRON RS7 Intelli IT Wrist Blood Pressure Monitor with Positioning Sensor and Bluetooth Connectivity for Use at Home or on the Go.
Taking your blood pressure readings shouldn’t be limited to when you can, at home. The RS7 Intelli IT helps you stay in control throughout the day – even when you’re on the go. Its small size and virtually silent pump mean it can be discreetly stored and used anywhere.