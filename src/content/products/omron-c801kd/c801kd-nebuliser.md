---
brand: "Omron"
type: "Nebuliser"
model: "C801kd"
title: "Omron c801kd Nebuliser"

---

OMRON CompAIR C801KD  is a compressor nebulizer for the treatment of lower airways respiratory diseases like asthma in babies and children.
It is specially designed for babies and children, thanks to its silent operation and child-friendly design.  CompAIR C801KD comes with a puppet accessory that can be attached to the device to make the inhalation more enjoyable. The device also comes with a child and infant mask for children and babies.

Designed for the treatment of lower airways conditions, with the particle size of 3.0 µm
Suitable for babies and children, with kid accessories and toys included
Efficient delivery of medication achieved through Virtual Valve Technology
Nebulization rate of 0.3 ml per minute
Silent operation at the noise level of 46 dB
Small, compact device
