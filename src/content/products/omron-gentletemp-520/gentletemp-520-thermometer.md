---
brand: "Omron"
type: "Thermometer"
model: "Gentletemp 520"
title: "Omron gentletemp 520 Thermometer"

---

The Gentle Temp 520 ear thermometer uses the same highly accurate technology as the Gentle Temp 521 and includes some of its advanced features, including one-second measurement and a memory for up to nine readings to track temperature changes over a period of time and monitor trends. It also comes with 21 probe covers. Temperatures can be displayed on the large LCD display in either degrees Centigrade or Fahrenheit.