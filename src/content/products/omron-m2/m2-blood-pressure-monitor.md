---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M2"
title: "Omron m2 BP Monitor"

---

Now you can comfortably, quickly and accurately check your blood pressure and monitor your results easily. Monitoring is made easy with the fully automatic digital OMRON M2 which features Intellisense Technology for soft cuff inflation. It is so easy to use you can take readings with just one touch.