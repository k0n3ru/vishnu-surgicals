---
brand: "Karma"
type: "Wheelchair"
model: "km-2512 Ergo lite"
title: "Karma km-2512 Ergo lite Wheelchair"
colors: "Silver Pearl"
sizes: "Attendant-propelled wheelchair with 14″ rear wheel or self-propelled wheelchair with 20″ (fixed or quick-released) rear wheel"
---

The Ergo Lite 2 (KM-2512) is an ultralight aluminum wheelchair with the ergonomic features of the Ergo Lite. Transfer easily on and off the wheelchair with the detachable swing-away footrest. You can choose between the attendant-propelled or the self-propelled type.