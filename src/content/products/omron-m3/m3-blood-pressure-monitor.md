---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M3"
title: "Omron m3 BP Monitor"

---

The M3 delivers the accuracy and reliability that you expect from OMRON blood pressure monitors, with additional advanced features to give you confidence in the quality and consistency of the results. The M3 is supplied with an Easy Cuff (22-42 cm), meaning it fits most adult arm sizes right out of the box.
The M3 is also designed to give you a reading that is easy to interpret. The Easy High Blood Pressure LED indicator signals if your blood pressure is higher than the normal range.
LED cuff wrap indicator confirms that cuff is wrapped correctly on your arm: not too loose or too tight.
Product features:

Intellisense Technology
Easy High Blood Pressure Colour Indicator
Irregular Heartbeat Detection
Average value
Body movement Detection
Cuff wrapping Guide
Blood pressure level indicator
2 user capability
60 memories for each user
