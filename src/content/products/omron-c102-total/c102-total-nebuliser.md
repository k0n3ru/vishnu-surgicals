---
brand: "Omron"
type: "Nebuliser"
model: "C102 total"
title: "Omron c102 total Nebuliser"

---

With the change of seasons, children can suffer from different respiratory diseases making it difficult to breathe freely.

In spring and summer pollen can trigger allergic rhinitis causing runny nose, sneezing or itchy eyes and nose
In autumn and winter viruses start spreading, affecting the airways with symptoms such as blocked or runny nose, coughing or a sore throat

With the OMRON C102 Total 2-in-1 Nebuliser with Nasal Shower, you can efficiently treat and provide relief for different respiratory symptoms and diseases throughout the year.

Nasal Shower - cleans the nasal cavity and helps to relieve symptoms of conditions such as common cold, allergic rhinitis or sinusitis
Nebuliser - provides efficient medication deposition in the lungs to treat diseases such as bronchitis, bronchiolitis and asthma
Easy to use and hygienic
Suitable for children and adults
