---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Rs1"
title: "Omron rs1 BP Monitor"

---

The OMRON RS1 automatic wrist blood pressure monitor is easy, portable and accurate for all body sizes.
Measuring your blood pressure from the wrist is a great alternative if you find upper arm cuffs either too small or uncomfortable. The RS1 cuff fits wrist sizes from 13.5 to 21.5 cm – which covers almost everyone. Just slip it on, raise your wrist to be level with your heart and wait for your result.
Small and compact, take the RS1 with you and keep on top of your blood pressure throughout the day.