---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M7 intelli it"
title: "Omron m7 intelli it BP Monitor"

---

OMRON M7 Intelli IT brings to market our latest innovation in accuracy: Intelli Wrap Cuff, while embedding our first OMRON smartphone app: OMRON connect.