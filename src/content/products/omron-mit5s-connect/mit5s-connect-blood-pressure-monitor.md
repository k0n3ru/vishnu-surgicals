---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Mit5s connect"
title: "Omron mit5s connect BP Monitor"

---

The MIT5s Connect combines advanced OMRON technology with modern design. Its unique combination of features ensures accurate measurement and enables to track blood pressure results on your smartphone anytime and anywhere.

Product Features:

OMRON connect makes it easy to view, save and manage your health data
Blood Pressure Level Indicator helps you to see at a glance if your blood pressure is high Innovative
Intellisense technology - Measurement without unnecessary over-inflation of the cuff
