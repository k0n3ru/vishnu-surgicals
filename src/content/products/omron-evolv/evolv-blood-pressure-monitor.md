---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Evolv"
title: "Omron evolv BP Monitor"

---

OMRON EVOLV - The new, easy to use, All in One Upper Arm Blood Pressure Monitor. Take accurate readings in any position around the upper arm1 & track progress via smartphone.