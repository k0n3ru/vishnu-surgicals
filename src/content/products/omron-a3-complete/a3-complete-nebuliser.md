---
brand: "Omron"
type: "Nebuliser"
model: "A3 complete"
title: "Omron a3 complete Nebuliser"

---

OMRON A3 Complete nebulizer provides the complete nebulization solution for a wide variety of respiratory conditions.
One single device can help you treat respiratory infections or chronic conditions of the upper and lower airways. This ranges from common cold infections or allergies to asthma or COPD.
For an inhaled medication to be most effective, it should reach the part of the airways that need to be treated. This is achieved by transforming the medication into an aerosol with different particle size.  Bigger aerosol particles will mainly be deposited to the upper airways, while smaller particles will be deposited to the lower airways.

Unique 3-in-1 adjustable nebulizer kit
Fast and efficient
Complete treatment of the upper, middle & lower airways
Allows for fast relief for upper airways
Small aerosol particles reach deep into the lungs
