---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Rs2"
title: "Omron rs2 BP Monitor"

---

Easy, portable and accurate for all body sizes

Intellisense Technology - Measurement without unnecessary over-inflation of the cuff
Cuff Wrap Guide - Indicates if the cuff is wrapped correctly: not too loose or too tight
Irregular heartbeat detection - Indicates if irregular heartbeat is detected during the reading
1 user x 30 memories
