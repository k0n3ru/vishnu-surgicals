---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M3 comfort"
title: "Omron m3 comfort BP Monitor"

---

The OMRON M3 Comfort comes with the Intelli Wrap Cuff technology, which supports 360º accuracy, enabling accurate results in any position around the upper arm.¹

Stroke prevention – Helps to detect stroke risk²
Clinically validated – Complies with the European Society of Hypertension standards
Intellisense Technology – Inflates the cuff to the ideal level for each use
Irregular Heartbeat detection – Indicates if irregular heartbeat is detected
Intelli Wrap Cuff Technology – 360° Accuracy. Accurate results in any position around the upper arm¹
Easy High Blood Pressure LED – Signals if your blood pressure is higher than the normal range

¹ Bilo G et al. Impact of cuff positioning on blood pressure measurement accuracy. May a specially designed cuff make a difference? Hypertens Res 2017. DOI: 10.1038/hr.2016.184
² Okubo T, Asayama K, Kikuya M, Metoki H, Obara T, Salto S, et al. Prediction on ischaemic and haemorrhagic stroke by self-measured blood pressure at home: The Ohasama study. Blood Press Monit. 2004; 9:315-320