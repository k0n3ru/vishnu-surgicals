---
brand: "Omron"
type: "Thermometer"
model: "Gentletemp 521"
title: "Omron gentletemp 521 Thermometer"

---

OMRON GentleTemp 521 ear thermometer is a super fast, digital thermometer with a range of sophisticated features including a screen that lights up. This means that it can be seen clearly at nighttime, making it especially handy for mothers looking after young children.