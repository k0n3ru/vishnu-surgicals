---
brand: "Omron"
type: "Nebuliser"
model: "Duobaby"
title: "Omron duobaby Nebuliser"

---

DuoBaby is a unique 2in1 compressor nebulizer with integrated nasal aspirator that can help treat the most common respiratory conditions of babies.
The nasal aspirator gently cleans the baby’s nose, reducing congestion and allowing him to breath freely. The nebulizer enables efficient medication delivery to the upper and lower airways.

2in1 nebulizer and nasal aspirator for babies.
The nasal aspirator helps to reduce nasal congestion and the risk of respiratory infections(1).
The nebulizer enables efficient delivery of medication for treating upper and lower airways conditions.  
Easy to use and hygienic.
The aspirator is fully washable with no need for antibacterial filters.

 
(1) Chirico G et al. Minerva Pediatrica 2014; 66 (6): 549-57