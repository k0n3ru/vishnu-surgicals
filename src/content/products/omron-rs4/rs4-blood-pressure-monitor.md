---
brand: "Omron"
type: "Blood Pressure monitor"
model: "Rs4"
title: "Omron rs4 BP Monitor"

---

Easy, portable and accurate for all body sizes

LED Positioning Sensor - for accurate results
Intellisense Technology - Measurement without unnecessary over-inflation of the cuff
Cuff Wrap Guide - Indicates if the cuff is wrapped correctly: not too loose or too tight
Irregular heartbeat detection - Indicates if irregular heartbeat is detected during the reading
Morning hypertension tracker
Hypertension Indicator – Indicates if hypertension is detected at the end of reading
1 user x 60 memories
