---
brand: "Omron"
type: "Nebuliser"
model: "Microair u100"
title: "Omron microair u100 Nebuliser"

---

The OMRON MicroAIR U100 is a small, portable nebuliser that helps to efficiently treat respiratory conditions from a simple cough to asthma.
The battery-operated MicroAIR U100 is compact, travel-ready and easy to reach for whenever you need it. The unique mesh technology makes it more efficient¹ and more comfortable to use than traditional nebulisers. It is virtually silent and the unique design enables usage in any position. Even while lying down, making it more comfortable for smaller children.