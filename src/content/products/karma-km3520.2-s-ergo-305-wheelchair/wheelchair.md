---
brand: "Karma"
type: "Wheelchair"
model: "S-Ergo 305 (KM-3520.2)"
title: "Karma S-Ergo 305 Wheelchair"
sizes: 16″, 18″ (seat width) 
---

Lightweight and sturdy, the S-Ergo 305 (KM-3520.2) is a premium choice for the self-propelled wheelchair. The S-Ergo seating system, flip-back armrests, swing-away footrests, and quick-release rear wheels are just a few amazing features that will enchance your experience. It has various features and adjustable components to suit the requirements of the wheelchair user.