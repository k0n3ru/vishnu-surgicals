---
brand: "Omron"
type: "Activity Monitor"
model: "Walking style one 2.1"
title: "Omron walking style one 2.1 Activity Monitor"

---

Research indicates that 10.000 steps a day helps ensure long-term health and reduces chronic disease risk. The Walking style One makes it simple to accurately keep track of your daily step count. The Walking style One 2.1 will also track the calories you have burned along the way and shows your more active (brisk) steps next to your normal walking routine.