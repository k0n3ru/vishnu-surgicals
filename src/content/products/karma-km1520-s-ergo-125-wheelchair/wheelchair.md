---
brand: "Karma"
type: "Wheelchair"
model: "km-1520 S Ergo lite"
title: "Karma S Ergo lite Wheelchair"
colors: "Silver Pearl, Rose Red"
---

Getting on and off a wheelchair can be a daily challenge for any wheelchair user. The S-Ergo 125 (KM-1520.3) is an aluminum wheelchair that comes equipped with detachable swing-away footrest and flip-back armrest to help the user meet that challenge. Add to that, the innovative S-Ergo seating system and you have a comfortable, convenient wheelchair at an affordable price.