---
brand: "Omron"
type: "Thermometer"
model: "Gentletemp 720"
title: "Omron gentletemp 720 Thermometer"

---

OMRON GentleTemp 720 contactless thermometer is a superfast, accurate infrared thermometer with specific features to keep your baby's temperature constantly under control, with the lowest possible fuss.