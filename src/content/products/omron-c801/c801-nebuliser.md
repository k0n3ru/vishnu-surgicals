---
brand: "Omron"
type: "Nebuliser"
model: "C801"
title: "Omron c801 Nebuliser"

---

The OMRON CompA.I.R™ NE-C801 is an every day compressor nebulizer for the treatment of lower airways respiratory diseases like asthma. With a low sound level, the CompA.I.R™ NE-C801 allows for a very quiet and comfortable operation The CompA.I.R™ nebulizer kit also features OMRON’s Virtual Valve Technology.