---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M2 basic"
title: "Omron m2 basic BP Monitor"

---

Now you can comfortably, quickly and accurately check your blood pressure and monitor your results easily. Monitoring is made easy with the fully automatic digital Omron M2 which features Intellisense Technology for soft cuff inflation. It is so easy to use you can take readings with just one touch.

Intellisense Technology - this unique OMRON technology ensures that there is less discomfort from over-inflation of the cuff.
Helps you take your blood pressure quickly and easily with one touch operation.
Included cuff fits arms with a circumference of 22-32cm
