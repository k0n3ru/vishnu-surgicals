---
brand: "Omron"
type: "Blood Pressure monitor"
model: "M6 comfort"
title: "Omron m6 comfort BP Monitor"

---

OMRON M6 Comfort comes with the Intelli Wrap Cuff technology, which supports 360 degrees accuracy, enabling accurate results in any position around the upper arm.

Stroke prevention – Helps to detect stroke risk
Clinically validated
Intellisense Technology – measurement without unpleasant feeling of pressure
Intelli Wrap Cuff Technology – 360 accuracy. Accurate results in any position around the upper arm.2
Blood Pressure Level Indicator – Indicates if result is above recommended level
Cuff Wrap Guide – Supports accurate  measurement
Irregular Heartbeat Detection
Averaging Function – Calculates the average of most recent values
Morning Averaging Function – Calculates the average of up to three measurements taken within 10 minutes of the first measurement of the morning (4:00- 11:59)
Evening Averaging Function – Calculates the average of up to three measurements taken within 10 minutes of the first measurement of the evening (19:00- 1:59)
Advanced Averaging Function – Weekly morning and evening averages
Body Movement Detection
Morning Hypertension Tracker – Indicates if morning average is above recommended level
Memory Capacity – 2 x 100
Storage bag included

 