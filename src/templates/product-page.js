import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import ProductGallery from '../components/product-gallery'
import { capitalize } from '../components/utils'
import Footer from '../components/footer'

import { Grid, Divider, Header, Icon, Table } from 'semantic-ui-react'

const ProductPropertiesRow = ({ name, value }) => {
  return (
    <Table.Row>
      <Table.Cell>{capitalize(name)}</Table.Cell>
      <Table.Cell>{value}</Table.Cell>
    </Table.Row>
  )
}

export default function Template(data) {
  const post = data.data.markdownRemark

  const keywords = Object.entries(post.frontmatter)
    .filter(([k, v]) => v)
    .filter(([k, v]) => k !== 'title')
    .map(([k, v]) => v)

  return (
    <Layout>
      <SEO title={post.frontmatter.title} keywords={keywords} />

      <Grid stackable columns={2}>
        <Grid.Column>
          <ProductGallery allFile={data.data.allFile} />
        </Grid.Column>
        <Grid.Column>
          <h2>{post.frontmatter.title}</h2>

          <React.Fragment>
            <Divider horizontal>
              <Header as="h4">
                <Icon name="tag" />
                Description
              </Header>
            </Divider>

            <p>{post.excerpt}</p>

            <Divider horizontal>
              <Header as="h4">
                <Icon name="bar chart" />
                Specifications
              </Header>
            </Divider>

            <Table definition>
              <Table.Body>
                {Object.entries(post.frontmatter)
                  .filter(([k, v]) => v)
                  .map(([k, v]) => {
                    return <ProductPropertiesRow key={k} name={k} value={v} />
                  })}
              </Table.Body>
            </Table>
          </React.Fragment>
        </Grid.Column>
      </Grid>


      <Footer />
    </Layout>
  )
}

export const postByPathQuery = graphql`
  query Post($id: String!, $relativeDirectory: String!) {
    markdownRemark(id: { eq: $id }) {
      excerpt(truncate: false, pruneLength: 10000, format: MARKDOWN)
      frontmatter {
        title
        brand
        model
        sizes
        colors
      }
    }
    allFile(
      filter: {
        extension: { regex: "/(png|jpeg|jpg)$/" }
        relativeDirectory: { eq: $relativeDirectory }
      }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxHeight: 300) {
              ...GatsbyImageSharpFluid
              presentationWidth
            }
          }
        }
      }
    }
  }
`
