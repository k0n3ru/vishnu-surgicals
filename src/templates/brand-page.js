import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import { ProductsList } from '../components/products-list'
import Footer from '../components/footer'
import { getDir, getImagesForFilter } from '../components/utils'

export default function Template(data) {
  const brandProducts = data.data.allMarkdownRemark.edges.map(e => {
    return {
      ...e.node.frontmatter,
      id: e.node.id,
      excerpt: e.node.excerpt,
      href: '/' + getDir(e.node.fileAbsolutePath),
      description: e.node.frontmatter.model + ' ' + e.node.frontmatter.type,
      images: getImagesForFilter(e.node.fileAbsolutePath, data.data.allFile),
    }
  })

  const brand = brandProducts[0].brand

  return (
    <Layout>
      <SEO
        title={'All products from ' + brand}
        keywords={[`brands`, `imported`, `quality`]}
      />
      <ProductsList data={brandProducts} />
      <Footer />
    </Layout>
  )
}

export const postByPathQuery = graphql`
  query ByBrand($brand: String!) {
    allMarkdownRemark(filter: { frontmatter: { brand: { eq: $brand } } }) {
      edges {
        node {
          id
          fileAbsolutePath
          frontmatter {
            title
            brand
            model
            type
            colors
            sizes
          }
          excerpt
          fileAbsolutePath
        }
      }
    }
    allFile(filter: { extension: { regex: "/(png|jpeg|jpg)$/" } }) {
      edges {
        node {
          id
          absolutePath
          base
          relativeDirectory
          relativePath
          childImageSharp {
            fluid(maxWidth: 150) {
              ...GatsbyImageSharpFluid
              presentationWidth
            }
            fixed(width: 200) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    }
  }
`
