/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
import {getDir, typeUrlForType, brandUrlForBrand} from './src/components/utils'
import path from 'path'

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions
  const productTemplate = path.resolve('src/templates/product-page.js')
  const brandTemplate = path.resolve('src/templates/brand-page.js')
  const typeTemplate = path.resolve('src/templates/product-type.js')
  return graphql(`
    {
      allMarkdownRemark {
        edges {
          node {
            fileAbsolutePath
            id
            frontmatter {
              type
              brand
            }
          }
        }
      }
    }
  `).then(res => {
      if (res.errors) {
        return Promise.reject(res.errors)
      }

      res.data.allMarkdownRemark.edges.forEach(({ node }) => {
        const relativeDirectory = getDir(node.fileAbsolutePath)
        const pagePath = '/' + relativeDirectory
        createPage({
          path: pagePath,
          component: productTemplate,
          context: {
            id: node.id,
            relativeDirectory: relativeDirectory,
          },
        })
      })

      const brands = new Set(res.data.allMarkdownRemark.edges.map(e => e.node.frontmatter.brand))
      brands.forEach(brand => {
          createPage({
            path: brandUrlForBrand(brand),
            component: brandTemplate,
            context: {
              brand: brand
            },
          })
        })

        const types = new Set(res.data.allMarkdownRemark.edges.map(e => e.node.frontmatter.type))
        types.forEach(type => {
            createPage({
              path: typeUrlForType(type),
              component: typeTemplate,
              context: {
                type: type
              },
            })
          })
  })
}
