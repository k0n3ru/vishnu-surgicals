import json
from omron import simple_get
import urllib
from bs4 import BeautifulSoup
from jinja2 import Template
import os
from slugify import slugify
import time

def start(main_url, product_category):
    raw_html = simple_get(main_url)
    soup = BeautifulSoup(raw_html, 'html.parser')
    schema_org_script = get_schema_script_tag(soup)
    product_urls = [el['url'] for el in json.loads(schema_org_script.text)['itemListElement']]
    [download_product_data(url, product_category) for url in product_urls]


def get_schema_script_tag(soup):
    return soup.find('script', type='application/ld+json')

def download_product_data(url, product_category):
    time.sleep(5)
    print("Downloading " + url)
    product_page = simple_get(url)
    product_soup = BeautifulSoup(product_page, 'html.parser')
    schema_org_script = get_schema_script_tag(product_soup)
    product_data = json.loads(schema_org_script.text)

    out = {
        'title': (product_data['brand']['name'] + ' ' + product_data['name']).capitalize() + " " + product_category,
        'type' : product_category,
        'model': product_data['name'].capitalize(),
        'brand' : product_data['brand']['name'].capitalize(),
        'description' : BeautifulSoup(product_data['description'], 'html.parser').get_text(),
        'images' : product_data['image']
    }
    save_to_content(out)

def save_to_content(out):
    destination = '../src/content/products/'
    dir_name = out['brand'] + "-" + out['model'] 
    dir_name = slugify(dir_name) 
    out_path = destination + "/"+ dir_name + "/"
    if not os.path.exists(out_path):
        os.makedirs(out_path)

    template_spec = '''---
brand: "{{brand}}"
type: "{{type}}"
model: "{{model}}"
title: "{{title}}"

---

{{description}}
'''

    template = Template(template_spec)
    markdown = template.render(out)
    file_p = out_path + slugify(out['model'] + '-' + out['type']) + ".md"
    md = open(file_p, 'w')
    md.write(markdown)

    [download_image(url, out_path) for url in out['images']]


def download_image(url, path):
    print(url)
    time.sleep(2)
    f = path + os.path.basename(url)
    urllib.request.urlretrieve(url,f)


#bp_monitors_url = 'https://www.omron-healthcare.com/eu/blood-pressure-monitors'
#start(bp_monitors_url, "BP Monitor")

# nebulizers_url = 'https://www.omron-healthcare.com/eu/nebulisers'
# start(nebulizers_url, "Nebuliser")

activity_monitors_url = 'https://www.omron-healthcare.com/eu/activity-monitors'
start(activity_monitors_url, "Activity Monitor")

thermometer_url = 'https://www.omron-healthcare.com/eu/thermometers'
start(thermometer_url, "Thermometer")

